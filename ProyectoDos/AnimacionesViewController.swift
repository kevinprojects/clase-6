//
//  AnimacionesViewController.swift
//  ProyectoDos
//
//  Created by Kevin Belter on 11/24/16.
//  Copyright © 2016 Kevin Belter. All rights reserved.
//

import UIKit

class AnimacionesViewController: UIViewController {

    @IBOutlet weak var lblResultado: UILabel!
    
    @IBAction func tocoCambiarColor() {
        //TODO: Programar la animacion.
        
        
        
        UIView.animate(withDuration: 1.5) {
            self.lblResultado.alpha = 0.2
        }
        
    }
}
