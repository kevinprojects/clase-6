//
//  Animaciones2ViewController.swift
//  ProyectoDos
//
//  Created by Kevin Belter on 11/24/16.
//  Copyright © 2016 Kevin Belter. All rights reserved.
//

import UIKit

class Animaciones2ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var pickerNombres: UIPickerView!
    @IBOutlet weak var pickerToBottom: NSLayoutConstraint!
    @IBOutlet weak var btnSeleccionar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerNombres.delegate = self
        pickerNombres.dataSource = self
    }
    
    @IBAction func tocoSeleccionarMoneda() {
        UIView.animate(withDuration: 0.3,
                       delay: 0.0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 1.5,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: {
                            self.pickerToBottom.constant = 0
                            self.view.layoutIfNeeded() },
                       completion: nil)
    }
    
    @IBAction func tocoSeleccionar() {
        let heightPicker = pickerNombres.bounds.height
        let heightBtnSeleccionar = btnSeleccionar.bounds.height
        let heightDistancia: CGFloat = 4.0
        
        let heightTotal = heightPicker + heightBtnSeleccionar + heightDistancia
        
        UIView.animate(withDuration: 0.3,
                       delay: 0.0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 1.5,
                       options: UIViewAnimationOptions.curveEaseOut,
                       animations: {
                                self.pickerToBottom.constant = heightTotal * -1
                                self.view.layoutIfNeeded()
                                },
                       completion: nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return nombres.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return nombres[row]
    }
    
    private var nombres = ["Pepe", "Jordan", "Dario", "Jorge Dudech", "Mario", "Kevin"]
}
